/* DO NOT EDIT THIS FILE - it is machine generated */
#include <jni.h>
#include <string.h>
/* Header for class com_example_ndkdemo_JniUtils */

#ifndef _Included_com_example_ndkdemo_JniUtils
#define _Included_com_example_ndkdemo_JniUtils
#ifdef __cplusplus
extern "C" {
#endif
/*
 * Class:     com_example_ndkdemo_JniUtils
 * Method:    getStringFromNative
 * Signature: ()Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_com_example_ndkdemo_JniUtils_getStringFromNative
  (JNIEnv *, jobject);

#ifdef __cplusplus
}
#endif
#endif

